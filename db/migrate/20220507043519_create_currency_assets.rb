class CreateCurrencyAssets < ActiveRecord::Migration[5.2]
  def change
    create_table :currency_assets do |t|
      t.string :currency
      t.string :name
      t.timestamps
    end
  end
end
