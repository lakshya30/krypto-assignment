class CreateUserKryptoAlerts < ActiveRecord::Migration[5.2]
  def change
    create_table :user_krypto_alerts do |t|
      t.string :user_id
      t.string :asset
      t.decimal :price
      t.string :currency_type
      t.string :status

      t.timestamps
    end
  end
end
