class AlertsController < ApplicationController
  before_action :authorize_request

  def create
    @alert = alert_params
    currency = CurrencyAsset.find_by(currency: @alert[:asset].downcase)
    if currency.nil?
      render json: { errors: "Invalid Currency. Valid currencies are #{CurrencyAsset.all.to_a}" }, status: :not_found
    end

    new_alert = UserKryptoAlert.new
    new_alert.user_id = @current_user.id
    new_alert.asset = @alert[:asset].upcase
    new_alert.price = @alert[:price]
    new_alert.currency_type = @alert[:asset].downcase
    new_alert.status = "CREATED"

    if new_alert.save
      ALERT_CACHE.set("#{@current_user.id}_#{new_alert.id}", new_alert.to_json)
      render json: new_alert, status: :ok
    end
  end

  def index
    al_params = all_alert_params
    pagy,records= pagy(UserKryptoAlert.where(user_id: @current_user.id).order("updated_at desc"), page: al_params[:page], items: al_params[:size])
    pagy.count
    render json: records, status: :ok
  end

  def delete
    alert  = UserKryptoAlert.where(user_id: @current_user.id, id: update_alert_params[:id]).first
    unless alert.present?
      render json: {errors: "No alert found"},status: :unprocessable_entity
    end
    alert.update(status: "DELETED")
    ALERT_CACHE.set("#{@current_user.id}_#{alert.id}", alert.to_json)
    render json: alert, status: :ok
  end


  private

  def alert_params
    params.permit(
      :asset, :price
    )
  end

  def all_alert_params
    params.permit(
      :page, :size
    )
  end

  def update_alert_params
    params.permit(
      :id
    )
  end
end
