class ApplicationController < ActionController::API
  include ActionController::RequestForgeryProtection
  include Pagy::Backend
  # before_action :authenticate_user
  # before_action :configure_permitted_parameters, if: :devise_controller?
  # protect_from_forgery with: :exception, unless: -> { request.format.json? }
  # respond_to :json
  # skip_before_action :verify_authenticity_token

  Pagy::DEFAULT[:overflow] = :empty_page


  def not_found
    render json: { error: 'not_found' }
  end

  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebTokenService.decode(header)
      @current_user = User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end
end
