Rails.application.routes.draw do
  # devise_for :users, controllers: { sessions: :sessions, registrations: :registrations}


  # devise_for :users, controllers: { sessions: 'users/sessions' , registrations: 'users/registrations' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :users, param: :_username
  post '/auth/login', to: 'authentication#login'
  # get '/*a', to: 'application#not_found'
  post '/alerts/create', to: 'alerts#create'
  get '/alerts', to: 'alerts#index'
  patch '/alerts', to: 'alerts#delete'
end
